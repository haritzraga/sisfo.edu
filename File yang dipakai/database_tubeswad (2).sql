-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Nov 2019 pada 04.13
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database_tubeswad`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `presensi`
--

CREATE TABLE `presensi` (
  `fileabsensi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_pengajar`
--

CREATE TABLE `table_pengajar` (
  `id` int(10) NOT NULL,
  `namadepan` varchar(200) NOT NULL,
  `namatengah` varchar(200) NOT NULL,
  `namabelakang` varchar(200) NOT NULL,
  `tempatlahir` varchar(200) NOT NULL,
  `tanggallahir` date NOT NULL,
  `email` varchar(200) NOT NULL,
  `nomorhp` int(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `matkul` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `table_pengajar`
--

INSERT INTO `table_pengajar` (`id`, `namadepan`, `namatengah`, `namabelakang`, `tempatlahir`, `tanggallahir`, `email`, `nomorhp`, `username`, `password`, `alamat`, `matkul`) VALUES
(1, 'Haritz', 'Raga', 'Pratama', 'Bengkulu', '1998-06-20', 'haritzraga123@email.com', 812345678, 'haritzpratama', 'raga123456', 'Bengkulu', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_users`
--

CREATE TABLE `table_users` (
  `id` int(200) NOT NULL,
  `namadepan` varchar(200) NOT NULL,
  `namatengah` varchar(200) NOT NULL,
  `namabelakang` varchar(200) NOT NULL,
  `tempatlahir` varchar(200) NOT NULL,
  `tanggallahir` date NOT NULL,
  `email` varchar(200) NOT NULL,
  `nomorhp` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `table_users`
--

INSERT INTO `table_users` (`id`, `namadepan`, `namatengah`, `namabelakang`, `tempatlahir`, `tanggallahir`, `email`, `nomorhp`, `username`, `password`, `alamat`) VALUES
(9, 'Nirwana', 'Anggie', 'Sinaga', '', '0000-00-00', 'wana@email.com', '', 'nirwana', 'wana123', ''),
(11, 'M', 'Zikri', 'Tau ah', '', '0000-00-00', 'zikri@email.com', '', 'zikri', 'zikri123', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tatatertib`
--

CREATE TABLE `tatatertib` (
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `table_pengajar`
--
ALTER TABLE `table_pengajar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `table_users`
--
ALTER TABLE `table_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `table_pengajar`
--
ALTER TABLE `table_pengajar`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `table_users`
--
ALTER TABLE `table_users`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
